module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    // TODO: add only when dev
    plugins: ["transform-class-properties", "istanbul"],
  };
};
