import { defineConfig } from "cypress";
import { createEsbuildPlugin } from "@badeball/cypress-cucumber-preprocessor/esbuild";
// @ts-ignore
import createBundler from "@bahmutov/cypress-esbuild-preprocessor";
import { NodeModulesPolyfillPlugin } from "@esbuild-plugins/node-modules-polyfill";
import { addCucumberPreprocessorPlugin } from "@badeball/cypress-cucumber-preprocessor";

export default defineConfig({
  e2e: {
    setupNodeEvents: async (on, config) => {
      require("@cypress/code-coverage/task")(on, config);

      await addCucumberPreprocessorPlugin(on, config);

      on(
        "file:preprocessor",
        createBundler({
          plugins: [NodeModulesPolyfillPlugin(), createEsbuildPlugin(config)],
        })
      );

      return config;
    },
    specPattern: "**/*.{feature,features}",
    baseUrl: "http://localhost:19006",
  },
  viewportWidth: 400,
  viewportHeight: 600,
  video: false,
});
