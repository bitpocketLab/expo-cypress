import {
  Given,
  When,
  Then,
  And,
} from "@badeball/cypress-cucumber-preprocessor";

Given("I land on home screen", () => {
  cy.visit("/");
});

Then("I can see {string}", (text: string) => {
  cy.get("div").contains(text);
});
